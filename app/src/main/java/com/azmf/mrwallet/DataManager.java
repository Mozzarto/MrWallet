package com.azmf.mrwallet;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DataManager {
    public static final int DATABASE_VERSION = 8;
    public static final String DATABASE_NAME = "mrwallet.db";
    private Context context;
    private SQLiteDatabase db;
    private MoneyDao moneyDao;
    private CategoryDao categoryDao;

    public DataManager(Context context) {
        this.context = context;
        SQLiteOpenHelper openHelper = new OpenHelper(this.context);
        db = openHelper.getWritableDatabase();
        moneyDao = new MoneyDao(db);
        categoryDao = new CategoryDao(db);
        categoryDao.save(new Category("Other", 0));
        categoryDao.save(new Category("Lottery", 0));
        categoryDao.save(new Category("Gift", 0));
        categoryDao.save(new Category("Sale", 0));
        categoryDao.save(new Category("Salary", 0));
        categoryDao.save(new Category("Return from a friend", 0));
        categoryDao.save(new Category("Tax refund", 0));
        categoryDao.save(new Category("Return in store", 0));
        categoryDao.save(new Category("Rent", 1));
        categoryDao.save(new Category("Electronics", 1));
        categoryDao.save(new Category("Food and drink", 1));
        categoryDao.save(new Category("Mandate", 1));
        categoryDao.save(new Category("Loan installment", 1));
        categoryDao.save(new Category("Entertainment", 1));
        categoryDao.save(new Category("Phone", 1));
        categoryDao.save(new Category("Health and beauty", 1));
        categoryDao.save(new Category("Clothes", 1));
        categoryDao.save(new Category("SMS", 1));
    }

    public List<Money> getAllMoney() {
        return moneyDao.getAll();
    }

    public List<Money> getAllIncome() {
        List<Category> allCategories = categoryDao.getAllIncome();
        List<Money> allMoney = moneyDao.getAll();
        final ArrayList<Money> moneyList = new ArrayList<>();
        for (Money money : allMoney) {
            for (Category category : allCategories) {
                if (category.getId() == money.getCategoryID())
                    moneyList.add(money);
            }
        }
        return moneyList;
    }

    public List<Money> getAllMoneyFromDay(String day) {
        return moneyDao.getAllFromDay(day);
    }
    public List<Money> getAllMoneyFromMonth(int month) {
        return moneyDao.getAllFromMonth(month);
    }

    public double getDayIncome(String day){
        double dayIncome = 0;
        List<Money> allMoney = moneyDao.getAllFromDay(day);
        List<Category> allCategories = categoryDao.getAllIncome();
        for (Money money : allMoney) {
            for (Category category : allCategories) {
                if (category.getId() == money.getCategoryID())
                    dayIncome += money.getAmount();
            }
        }
        return dayIncome;
    }

    public double getDayOutcome(String day){
        double dayOutcome = 0;
        List<Money> allMoney = moneyDao.getAllFromDay(day);
        List<Category> allCategories = categoryDao.getAllOutcome();
        for (Money money : allMoney) {
            for (Category category : allCategories) {
                if (category.getId() == money.getCategoryID())
                    dayOutcome += money.getAmount();
            }
        }
        return dayOutcome;
    }

    public double getDayBalance(String day){
        return getDayIncome(day)-getDayOutcome(day);
    }

    public List<Money> getAllOutcome() {
        List<Category> allCategories = categoryDao.getAllOutcome();
        List<Money> allMoney = moneyDao.getAll();
        final ArrayList<Money> moneyList = new ArrayList<>();
        for (Money money : allMoney) {
            for (Category category : allCategories) {
                if (category.getId() == money.getCategoryID())
                    moneyList.add(money);
            }
        }
        return moneyList;
    }

    public double getYearIncome(int year){
        double yearIncome = 0;
        List<Money> allMoney = moneyDao.getAllFromYear(year);
        List<Category> allCategories = categoryDao.getAllIncome();
        for (Money money : allMoney) {
            for (Category category : allCategories) {
                if (category.getId() == money.getCategoryID())
                    yearIncome += money.getAmount();
            }
        }
        return yearIncome;
    }

    public double getYearOutcome(int year){
        double yearOutcome = 0;
        List<Money> allMoney = moneyDao.getAllFromYear(year);
        List<Category> allCategories = categoryDao.getAllOutcome();
        for (Money money : allMoney) {
            for (Category category : allCategories) {
                if (category.getId() == money.getCategoryID())
                    yearOutcome += money.getAmount();
            }
        }
        return yearOutcome;
    }

    public double getYearBalance(int year){
        return getYearIncome(year)-getYearOutcome(year);
    }

    public double getMonthIncome(int month){
        double monthIncome = 0;
        List<Money> allMoney = moneyDao.getAllFromMonth(month);
        List<Category> allCategories = categoryDao.getAllIncome();
        for (Money money : allMoney) {
            for (Category category : allCategories) {
                if (category.getId() == money.getCategoryID())
                    monthIncome += money.getAmount();
            }
        }
        return monthIncome;
    }

    public double getMonthOutcome(int month){
        double monthOutcome = 0;
        List<Money> allMoney = moneyDao.getAllFromMonth(month);
        List<Category> allCategories = categoryDao.getAllOutcome();
        for (Money money : allMoney) {
            for (Category category : allCategories) {
                if (category.getId() == money.getCategoryID())
                    monthOutcome += money.getAmount();
            }
        }
        return monthOutcome;
    }

    public List<Money> getMonthIncomeList(int month){
        double value = 0;
        List<Category> allCategories = categoryDao.getAllIncome();
        List<Money> allMoney = moneyDao.getAllFromMonthGroupByCategory(month);
        final ArrayList<Money> moneyList = new ArrayList<>();
        if (!allMoney.isEmpty()) {
            for (Category category : allCategories) {
                for (Money money : allMoney) {
                    if (category.getId() == money.getCategoryID())
                        value += money.getAmount();
                }
                if (value != 0)
                    moneyList.add(new Money(value, "", category.getId()));
                value = 0;
            }
        }
        return moneyList;
    }

    public List<Money> getMonthOutcomeList(int month){
        double value = 0;
        List<Category> allCategories = categoryDao.getAllOutcome();
        List<Money> allMoney = moneyDao.getAllFromMonthGroupByCategory(month);
        final ArrayList<Money> moneyList = new ArrayList<>();
        if (!allMoney.isEmpty()) {
            for (Category category : allCategories) {
                for (Money money : allMoney) {
                    if (category.getId() == money.getCategoryID())
                        value += money.getAmount();
                }
                if (value != 0)
                    moneyList.add(new Money(value, "", category.getId()));
                value = 0;
            }
        }
        return moneyList;
    }

    public double getMonthBalance(int month){
        return getMonthIncome(month)-getMonthOutcome(month);
    }

    public Money getMoney(long movieId) {
        return moneyDao.get(movieId);
    }

    public Money findMoney(long categoryId) {
        return moneyDao.find(categoryId);
    }

    public long saveMoney(Money money) {
        return moneyDao.save(money);
    }

    public long saveCategory(Category category) {
        return categoryDao.save(category);
    }

    public void deleteCategory(Category category) {
        categoryDao.delete(category);
    }

    public Category getCategory(long categoryId) {
        return categoryDao.get(categoryId);
    }

    public List<Category> getAllCategories() {
        return categoryDao.getAll();
    }

    public List<Category> getAllIncomeCategories() {
        return categoryDao.getAllIncome();
    }

    public List<Category> getAllOutcomeCategories() {
        return categoryDao.getAllOutcome();
    }

    public Category findCategory(String name) {
        return categoryDao.find(name);
    }
}
