package com.azmf.mrwallet;

import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class CategoryTable {
    public static final String TABLE_NAME = "category";

    public static class CategoryColumns implements BaseColumns {
        public static final String NAME = "name";
        public static final String TYPE = "type";
    }

    public static void onCreate(SQLiteDatabase db) {
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE " + CategoryTable.TABLE_NAME + " (");
        sb.append(BaseColumns._ID + " INTEGER PRIMARY KEY, ");
        sb.append(CategoryColumns.NAME + " TEXT, ");
        sb.append(CategoryColumns.TYPE + " INTEGER ");
        sb.append(");");
        db.execSQL(sb.toString());
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "
                + CategoryTable.TABLE_NAME);
        CategoryTable.onCreate(db);
    }
}
