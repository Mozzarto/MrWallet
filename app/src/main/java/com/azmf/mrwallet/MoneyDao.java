package com.azmf.mrwallet;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MoneyDao implements Dao<Money>{
    private static final String INSERT = "insert into "
            + MoneyTable.TABLE_NAME
            + "(" + MoneyTable.MoneyColumns.AMOUNT
            + ", " + MoneyTable.MoneyColumns.DATE
            + ", " + MoneyTable.MoneyColumns.CATEGORY_ID
            + ") values(?, ?, ?)";

    private SQLiteDatabase db;
    private SQLiteStatement insertStatement;

    public MoneyDao(SQLiteDatabase db) {
        this.db = db;
        insertStatement = db.compileStatement(MoneyDao.INSERT);
    }

    @Override
    public long save(Money entity) {
        insertStatement.clearBindings();
        insertStatement.bindDouble(1, entity.getAmount());
        insertStatement.bindString(2, entity.getDate());
        insertStatement.bindLong(3, entity.getCategoryID());
        return insertStatement.executeInsert();
    }

    @Override
    public List<Money> getAll() {
        List<Money> list = new ArrayList<>();
        Cursor c = db.query(
                MoneyTable.TABLE_NAME,
                new String[]{
                        BaseColumns._ID,
                        MoneyTable.MoneyColumns.AMOUNT,
                        MoneyTable.MoneyColumns.DATE,
                        MoneyTable.MoneyColumns.CATEGORY_ID
                },
                null, null, null, null, MoneyTable.MoneyColumns.DATE, null);
        if (c.moveToFirst()) {
            do {
                Money money = this.buildMoneyFromCursor(c);
                if (money != null) {
                    list.add(money);
                }
            } while (c.moveToNext());
        }
        if (!c.isClosed()) {
            c.close();
        }
        return list;
    }

    public List<Money> getAllFromYear(int year) {
        List<Money> list = new ArrayList<>();
        Cursor c = db.query(
                MoneyTable.TABLE_NAME,
                new String[]{
                        BaseColumns._ID,
                        MoneyTable.MoneyColumns.AMOUNT,
                        MoneyTable.MoneyColumns.DATE,
                        MoneyTable.MoneyColumns.CATEGORY_ID
                },
                null, null, null, null, MoneyTable.MoneyColumns.DATE, null);
        if (c.moveToFirst()) {
            do {
                Money money = this.buildMoneyFromCursor(c);
                if (money != null && money.getDate().split("\\.")[2].equals(String.valueOf(year))) {
                    list.add(money);
                }
            } while (c.moveToNext());
        }
        if (!c.isClosed()) {
            c.close();
        }
        return list;
    }

    public List<Money> getAllFromMonth(int month) {
        List<Money> list = new ArrayList<>();
        Cursor c = db.query(
                MoneyTable.TABLE_NAME,
                new String[]{
                        BaseColumns._ID,
                        MoneyTable.MoneyColumns.AMOUNT,
                        MoneyTable.MoneyColumns.DATE,
                        MoneyTable.MoneyColumns.CATEGORY_ID
                },
                null, null, null, null, MoneyTable.MoneyColumns.DATE, null);
        if (c.moveToFirst()) {
            do {
                Money money = this.buildMoneyFromCursor(c);
                if (money != null && money.getDate().split("\\.")[1].equals(String.valueOf(month))) {
                    list.add(money);
                }
            } while (c.moveToNext());
        }
        if (!c.isClosed()) {
            c.close();
        }

        Collections.reverse(list);

        return list;
    }

    public List<Money> getAllFromDay(String month) {
        List<Money> list = new ArrayList<>();
        Cursor c = db.query(
                MoneyTable.TABLE_NAME,
                new String[]{
                        BaseColumns._ID,
                        MoneyTable.MoneyColumns.AMOUNT,
                        MoneyTable.MoneyColumns.DATE,
                        MoneyTable.MoneyColumns.CATEGORY_ID
                },
                null, null, null, null, MoneyTable.MoneyColumns.DATE, null);
        if (c.moveToFirst()) {
            do {
                Money money = this.buildMoneyFromCursor(c);
                if (money != null && money.getDate().split("\\.")[1].equals(month)) {
                    list.add(money);
                }
            } while (c.moveToNext());
        }
        if (!c.isClosed()) {
            c.close();
        }
        return list;
    }

    public List<Money> getAllFromMonthGroupByCategory(int month) {
        List<Money> list = new ArrayList<>();
        Cursor c = db.query(
                MoneyTable.TABLE_NAME,
                new String[]{
                        BaseColumns._ID,
                        MoneyTable.MoneyColumns.AMOUNT,
                        MoneyTable.MoneyColumns.DATE,
                        MoneyTable.MoneyColumns.CATEGORY_ID
                },
                null, null, null, null, MoneyTable.MoneyColumns.DATE, null);
        if (c.moveToFirst()) {
            do {
                Money money = this.buildMoneyFromCursor(c);
                if (money != null && money.getDate().split("\\.")[1].equals(String.valueOf(month))) {
                    list.add(money);
                }
            } while (c.moveToNext());
        }
        if (!c.isClosed()) {
            c.close();
        }
        return list;
    }

    @Override
    public Money get(long id) {
        Money money = null;
        Cursor c =
                db.query(MoneyTable.TABLE_NAME,
                        new String[] {
                                BaseColumns._ID,
                                MoneyTable.MoneyColumns.AMOUNT,
                                MoneyTable.MoneyColumns.DATE,
                                MoneyTable.MoneyColumns.CATEGORY_ID},
                        BaseColumns._ID + " = ?", new String[] { String.valueOf(id) },
                        null, null, null, "1");
        if (c.moveToFirst()) {
            money = this.buildMoneyFromCursor(c);
        }
        if (!c.isClosed()) {
            c.close();
        }
        return money;
    }

    public Money find(long categoryId) {
        Money money = null;
        Cursor c =
                db.query(MoneyTable.TABLE_NAME,
                        new String[] {
                                BaseColumns._ID,
                                MoneyTable.MoneyColumns.AMOUNT,
                                MoneyTable.MoneyColumns.DATE,
                                MoneyTable.MoneyColumns.CATEGORY_ID},
                        MoneyTable.MoneyColumns.CATEGORY_ID + " = ?", new String[] { String.valueOf(categoryId) },
                        null, null, null, "1");
        if (c.moveToFirst()) {
            money = this.buildMoneyFromCursor(c);
        }
        if (!c.isClosed()) {
            c.close();
        }
        return money;
    }

    private Money buildMoneyFromCursor(Cursor c) {
        Money money = null;
        if (c != null) {
            money = new Money();
            money.setId(c.getLong(0));
            money.setAmount(c.getDouble(1));
            money.setDate(c.getString(2));
            money.setCategoryID(c.getLong(3));
        }
        return money;
    }

    @Override
    public void delete(Money entity) {
        if (entity.getId() > 0) {
            db.delete(MoneyTable.TABLE_NAME,
                    BaseColumns._ID + " = ?", new String[]
                            { String.valueOf(entity.getId()) });
        }
    }

    @Override
    public void update(Money entity) {
        final ContentValues values = new ContentValues();
        values.put(MoneyTable.MoneyColumns.AMOUNT, entity.getAmount());
        values.put(MoneyTable.MoneyColumns.DATE, entity.getDate());
        values.put(MoneyTable.MoneyColumns.CATEGORY_ID, entity.getCategoryID());
        db.update(MoneyTable.TABLE_NAME, values,
                BaseColumns._ID + " = ?", new String[] {
                        String.valueOf(entity.getId()) });
    }
}
