package com.azmf.mrwallet;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.Calendar;

public class SmsReciver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
            Bundle bundle = intent.getExtras();           //---get the SMS message passed in---
            SmsMessage msgs = null;
            String msgBody = "";
            if (bundle != null){
                //---retrieve the SMS message received---
                try{
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    //msgs = new SmsMessage[pdus.length];
                    if(pdus.length > 0) {
                        msgs = SmsMessage.createFromPdu((byte[])pdus[pdus.length - 1]);
                        msgBody = msgs.getMessageBody();

                        int year = Calendar.getInstance().get(Calendar.YEAR);
                        int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
                        int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
                        String date = day + "." + month + "." + year;

                        double money = getMoneyFromText(msgBody);
                        if(money != -1){
                            DataManager dataManager = DataManagerObject.dataManager;
                            Category category = dataManager.findCategory("SMS");
                            long categoryId = category.getId();
                            Money moneyObject = new Money(money, date, categoryId);
                            dataManager.saveMoney(moneyObject);
                        }
                        //Toast.makeText(context, Double.toString(getMoneyFromText(msgBody)), Toast.LENGTH_LONG).show();
                    }
                }catch(Exception e){
//                            Log.d("Exception caught",e.getMessage());
                }
            }
        }
    }

    /**
     *
     * @param txt
     * @return -1 error;  > 0 OK
     */
    private double getMoneyFromText(String txt){
        boolean coma = false;
        int gr = 0;
        int zl = 0;
        int mn = 1;
        for(int i = txt.length() - 1; i >= 0; i--){
            if(txt.charAt(i) == 'ł'){
                if(txt.charAt(i - 1) == 'z'){
                    for(int j = i - 2; j >= 0; j--){
                        if(txt.charAt(j) == ' '){
                            continue;
                        } else if(txt.charAt(j) >= '0' && txt.charAt(j)  <= '9'){
                            if(!coma){
                                gr = gr + (txt.charAt(j) - '0') * mn;
                            } else {
                                zl = zl + (txt.charAt(j) - '0') * mn;
                            }
                            mn *= 10;
                        } else if(txt.charAt(j) == '.' || txt.charAt(j) == ','){
                            coma = true;
                            mn = 1;
                        } else {
                            break;
                        }
                    }
                    break;
                }
            }
        }

        if(!coma){
            if(gr == 0){
                return -1;
            } else {
                return gr;
            }
        } else {
            if(gr/100 != 0){
                return -1;
            } else {
                return zl + gr/100.;
            }
        }
    }
}
