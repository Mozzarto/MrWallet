package com.azmf.mrwallet;

import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class MoneyTable {
    public static final String TABLE_NAME = "money";

    public static class MoneyColumns implements BaseColumns {
        public static final String AMOUNT = "amount";
        public static final String DATE = "date";
        public static final String CATEGORY_ID = "categoryID";
    }

    public static void onCreate(SQLiteDatabase db) {
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE " + MoneyTable.TABLE_NAME + " (");
        sb.append(BaseColumns._ID + " INTEGER PRIMARY KEY, ");
        sb.append(MoneyTable.MoneyColumns.AMOUNT + " INTEGER, ");
        sb.append(MoneyColumns.DATE + " TEXT, ");
        sb.append(MoneyColumns.CATEGORY_ID + " INTEGER ");
        sb.append(");");
        db.execSQL(sb.toString());
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "
                + MoneyTable.TABLE_NAME);
        MoneyTable.onCreate(db);
    }
}
