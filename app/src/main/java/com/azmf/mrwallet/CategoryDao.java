package com.azmf.mrwallet;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.List;

public class CategoryDao implements Dao<Category> {
    private static final String INSERT = "insert into "
            + CategoryTable.TABLE_NAME
            + "(" + CategoryTable.CategoryColumns.NAME
            + ", " + CategoryTable.CategoryColumns.TYPE
            + ") values(?, ?)";

    private SQLiteDatabase db;
    private SQLiteStatement insertStatement;

    public CategoryDao(SQLiteDatabase db) {
        this.db = db;
        insertStatement = db.compileStatement(CategoryDao.INSERT);
    }

    @Override
    public long save(Category entity) {
        insertStatement.clearBindings();
        insertStatement.bindString(1, entity.getName());
        insertStatement.bindDouble(2, entity.getType());
        return insertStatement.executeInsert();
    }

    @Override
    public List<Category> getAll() {
        List<Category> list = new ArrayList<Category>();
        Cursor c = db.query(
                CategoryTable.TABLE_NAME,
                new String[]{
                        BaseColumns._ID,
                        CategoryTable.CategoryColumns.NAME,
                        CategoryTable.CategoryColumns.TYPE
                },
                null, null, CategoryTable.CategoryColumns.TYPE, null,
                CategoryTable.CategoryColumns.NAME, null);
        if (c.moveToFirst()) {
            do {
                Category category = this.buildCategoryFromCursor(c);
                if (category != null) {
                    list.add(category);
                }
            } while (c.moveToNext());
        }
        if (!c.isClosed()) {
            c.close();
        }
        return list;
    }

    public List<Category> getAllIncome() {
        List<Category> list = new ArrayList<Category>();
        Cursor c = db.query(
                CategoryTable.TABLE_NAME,
                new String[]{
                        BaseColumns._ID,
                        CategoryTable.CategoryColumns.NAME,
                        CategoryTable.CategoryColumns.TYPE
                },
                CategoryTable.CategoryColumns.TYPE + " = ?", new String[] { String.valueOf(0) },
                null, null, CategoryTable.CategoryColumns.NAME, null);
        if (c.moveToFirst()) {
            do {
                Category category = this.buildCategoryFromCursor(c);
                if (category != null) {
                    list.add(category);
                }
            } while (c.moveToNext());
        }
        if (!c.isClosed()) {
            c.close();
        }
        return list;
    }

    public List<Category> getAllOutcome() {
        List<Category> list = new ArrayList<Category>();
        Cursor c = db.query(
                CategoryTable.TABLE_NAME,
                new String[]{
                        BaseColumns._ID,
                        CategoryTable.CategoryColumns.NAME,
                        CategoryTable.CategoryColumns.TYPE
                },
                CategoryTable.CategoryColumns.TYPE + " = ?", new String[] { String.valueOf(1) },
                null, null, CategoryTable.CategoryColumns.NAME, null);
        if (c.moveToFirst()) {
            do {
                Category category = this.buildCategoryFromCursor(c);
                if (category != null) {
                    list.add(category);
                }
            } while (c.moveToNext());
        }
        if (!c.isClosed()) {
            c.close();
        }
        return list;
    }

    @Override
    public Category get(long id) {
        Category category = null;
        Cursor c =
                db.query(CategoryTable.TABLE_NAME,
                        new String[] {
                                BaseColumns._ID,
                                CategoryTable.CategoryColumns.NAME,
                                CategoryTable.CategoryColumns.TYPE},
                        BaseColumns._ID + " = ?", new String[] { String.valueOf(id) },
                        null, null, null, "1");
        if (c.moveToFirst()) {
            category = this.buildCategoryFromCursor(c);
        }
        if (!c.isClosed()) {
            c.close();
        }
        return category;
    }

    public Category find(String name) {
        long categoryId = 0L;
        String sql = "select _id from " + CategoryTable.TABLE_NAME
                + " where upper(" + CategoryTable.CategoryColumns.NAME + ") = ? limit 1";
        Cursor c = db.rawQuery(sql,
                new String[]
                        {name.toUpperCase()});
        if (c.moveToFirst()) {
            categoryId = c.getLong(0);
        }
        if (!c.isClosed()) {
            c.close();
        }
        return this.get(categoryId);
    }

    private Category buildCategoryFromCursor(Cursor c) {
        Category category = null;
        if (c != null) {
            category = new Category();
            category.setId(c.getLong(0));
            category.setName(c.getString(1));
            category.setType(c.getInt(2));
        }
        return category;
    }

    @Override
    public void delete(Category entity) {
        if (entity.getId() > 0) {
            db.delete(CategoryTable.TABLE_NAME,
                    BaseColumns._ID + " = ?", new String[]
                            { String.valueOf(entity.getId()) });
        }
    }

    @Override
    public void update(Category entity) {
        final ContentValues values = new ContentValues();
        values.put(CategoryTable.CategoryColumns.NAME, entity.getName());
        values.put(CategoryTable.CategoryColumns.TYPE, entity.getType());
        db.update(CategoryTable.TABLE_NAME, values,
                BaseColumns._ID + " = ?", new String[] {
                        String.valueOf(entity.getId()) });
    }
}
