package com.azmf.mrwallet;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.view.View;
import android.widget.TextView;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import com.azmf.mrwallet.fragment.EntryFragment;
import com.azmf.mrwallet.fragment.MoneyFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final int SMS_PERMISSION_CODE = 100;
    double balance, income, outgoing;
    ArrayList<MoneyFragment> list = new ArrayList<>();
    private static DecimalFormat df2 = new DecimalFormat("0.00");
    private DataManager dataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FloatingActionButton fab = findViewById(R.id.addButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, AddingElementActivity.class));
            }
        });

        FloatingActionButton summary = findViewById(R.id.summaryButton);
        summary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Summary.class));
            }
        });

        FloatingActionButton refresh = findViewById(R.id.refreshButton);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
                startActivity(getIntent());
            }
        });

        //Service
        Intent intent = new Intent(this, ToastService.class);
        startService(intent);
        this.createDataManager();
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
        int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        String date = day + "." + month + "." + year;
        dataManager = DataManagerObject.dataManager;
        addCategoriesToDatabase();
        balance = dataManager.getMonthBalance(month);
        income = dataManager.getMonthIncome(month);
        outgoing = dataManager.getMonthOutcome(month);

        List<Money> listMoney = dataManager.getAllMoneyFromMonth(month);

        for (Money money : listMoney){
            Category category = dataManager.getCategory(money.getCategoryID());
            list.add(new MoneyFragment(money.getId(), money.getAmount(), money.getDate(), category.getName()));
        }

        //Check permisions
        if (!isSmsPermissionGranted()) {
            requestReadAndSendSmsPermission();
        }

        dataToView();
    }

    public boolean isSmsPermissionGranted() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Request runtime SMS permission
     */
    private void requestReadAndSendSmsPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_SMS)) {
            // You may display a non-blocking explanation here, read more in the documentation:
            // https://developer.android.com/training/permissions/requesting.html
        }
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS}, SMS_PERMISSION_CODE);
    }

    private void createDataManager(){
        if(DataManagerObject.dataManager == null)
            DataManagerObject.dataManager = new DataManager(this.getApplicationContext());
    }

    //Method for setting values to view
    private void dataToView() {
        TextView balanceTV = findViewById(R.id.MainBalanceValue);
        balanceTV.setText(doubleToString(balance));

        TextView incomeTV = findViewById(R.id.MainInComeValue);
        incomeTV.setText(doubleToString(income));

        TextView outgoingTV = findViewById(R.id.MainOutComeValue);
        outgoingTV.setText(doubleToString(outgoing));

        EntryFragment entrFrag = (EntryFragment) getSupportFragmentManager().findFragmentById(R.id.mainFragment);
        if(entrFrag != null)
            entrFrag.passList(list);

    }

    private String doubleToString(double money) {
        String ret = df2.format(money);
        ret += " zł";
        return ret;
    }

    private void addCategoriesToDatabase(){

        String[] incomeCat = getApplicationContext().getResources().getStringArray(R.array.incomeCategories);
        String[] outgoingCat = getApplicationContext().getResources().getStringArray(R.array.outgoingCategories);

        for (Category cat : dataManager.getAllIncomeCategories()) {
            if (dataManager.findCategory(cat.getName()) == null) {
                dataManager.saveCategory(new Category("Other", 0));
                dataManager.saveCategory(new Category("Lottery", 0));
                dataManager.saveCategory(new Category("Gift", 0));
                dataManager.saveCategory(new Category("Sale", 0));
                dataManager.saveCategory(new Category("Salary", 0));
                dataManager.saveCategory(new Category("Return from a friend", 0));
                dataManager.saveCategory(new Category("Tax refund", 0));
                dataManager.saveCategory(new Category("Return in store", 0));
            }
        }
        for (Category cat : dataManager.getAllOutcomeCategories()) {
            if (dataManager.findCategory(cat.getName()) == null) {
                dataManager.saveCategory(new Category("Rent", 1));
                dataManager.saveCategory(new Category("Electronics", 1));
                dataManager.saveCategory(new Category("Food and drink", 1));
                dataManager.saveCategory(new Category("Mandate", 1));
                dataManager.saveCategory(new Category("Loan installment", 1));
                dataManager.saveCategory(new Category("Entertainment", 1));
                dataManager.saveCategory(new Category("Phone", 1));
                dataManager.saveCategory(new Category("Health and beauty", 1));
                dataManager.saveCategory(new Category("Clothes", 1));
                dataManager.saveCategory(new Category("SMS", 1));
            }
        }
    }

}
