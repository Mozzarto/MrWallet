package com.azmf.mrwallet;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;

public class AddingElementActivity extends Activity{
    private Spinner spinnerType;
    private Spinner spinnerCategory;
    private SharedPreferences preferences;
    private Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.adding_element);

        final DataManager dataManager = DataManagerObject.dataManager;

        spinnerType = findViewById(R.id.type);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(AddingElementActivity.this,
                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.typeElement));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerType.setAdapter(adapter);
        spinnerCategory = findViewById(R.id.category);
        preferences = getSharedPreferences("MyPref", Activity.MODE_PRIVATE);
        restoreDataType();

        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch(i){
                    case 0:
                        //List<Category> categoryInList = dataManager.getAllIncomeCategories();
                        String[] categoryInList = getApplicationContext().getResources().getStringArray(R.array.incomeCategories);
                        ArrayList<String> categoryInArr = new ArrayList<>();
                        for (String category : categoryInList) {
                            categoryInArr.add(category);
                        }
                        ArrayAdapter<String> adapterIn = new ArrayAdapter<>(getApplicationContext(),
                                android.R.layout.simple_spinner_item, categoryInArr);
                        spinnerCategory.setAdapter(adapterIn);
                        break;
                    case 1:
                        //List<Category> categoryOutList = dataManager.getAllOutcomeCategories();
                        String[] categoryOutList = getApplicationContext().getResources().getStringArray(R.array.outgoingCategories);

                        ArrayList<String> categoryOutArr = new ArrayList<>();
                        for (String category : categoryOutList) {
                            categoryOutArr.add(category);
                        }
                        ArrayAdapter<String> adapterOut = new ArrayAdapter<>(getApplicationContext(),
                                android.R.layout.simple_spinner_item, categoryOutArr);
                        spinnerCategory.setAdapter(adapterOut);
                        break;
                }
                restoreDataCategory();
                saveData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                saveData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        saveButton = findViewById(R.id.save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            double moneyValue;
            String dateValue;
            Category categoryItem;
            String categoryName;
            EditText moneyEditText;
            EditText dateEditText;

            @Override
            public void onClick(View view)
            {
                moneyEditText = findViewById(R.id.money);

                if(moneyEditText.getText().length() != 0){
                    moneyValue = Double.parseDouble(moneyEditText.getText().toString());
                }
                else{
                    moneyValue = 0.0;
                }

                dateEditText = findViewById(R.id.date);
                if(dateEditText.getText().length() != 0){
                    dateValue = dateEditText.getText().toString();
                }
                else{
                    dateValue = "nothing";
                }

                if(moneyValue == 0.0 || dateValue == "nothing"){
                    Toast.makeText(AddingElementActivity.this, getResources().getString(R.string.completeFields), Toast.LENGTH_SHORT).show();
                }
                else {
                    categoryName = spinnerCategory.getSelectedItem().toString();

                    switch (categoryName) {
                        case "Inne":
                            categoryName = "Other";
                            break;
                        case "Loteria":
                            categoryName = "Lottery";
                            break;
                        case "Prezent":
                            categoryName = "Gift";
                            break;
                        case "Sprzedaż":
                            categoryName = "Sale";
                            break;
                        case "Wypłata":
                            categoryName = "Salary";
                            break;
                        case "Zwrot od przyjaciela":
                            categoryName = "Return from a friend";
                            break;
                        case "Zwrot podatku":
                            categoryName = "Tax refund";
                            break;
                        case "Zwrot w sklepie":
                            categoryName = "Return in store";
                            break;
                        case "Czynsz":
                            categoryName = "Rent";
                            break;
                        case "Elektronika":
                            categoryName = "Electronics";
                            break;
                        case "Jedzenie i picie":
                            categoryName = "Food and drink";
                            break;
                        case "Mandat":
                            categoryName = "Mandate";
                            break;
                        case "Rata":
                            categoryName = "Loan installment";
                            break;
                        case "Rozrywka":
                            categoryName = "Entertainment";
                            break;
                        case "Telefon":
                            categoryName = "Phone";
                            break;
                        case "Zdrowie i uroda":
                            categoryName = "Health and beauty";
                            break;
                        case "Odzież":
                            categoryName = "Clothes";
                            break;
                        case "SMS":
                            categoryName = "SMS";
                            break;
                    }

                    categoryItem = dataManager.findCategory(categoryName);
                    Money money = new Money(moneyValue, dateValue, categoryItem.getId());

                    long result = dataManager.saveMoney(money);

                    if ((int) result != 0) {
                        Toast.makeText(AddingElementActivity.this, getResources().getString(R.string.completed), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(AddingElementActivity.this, MainActivity.class));
                    }
                    else {
                        Toast.makeText(AddingElementActivity.this, getResources().getString(R.string.error), Toast.LENGTH_SHORT).show();
                    }
                }
                saveData();
            }
        });
    }

    private void saveData() {
        SharedPreferences.Editor preferencesEditor = preferences.edit();
        int spinnerTypeElement = spinnerType.getSelectedItemPosition();
        int spinnerCategoryElement = spinnerCategory.getSelectedItemPosition();
        preferencesEditor.putInt("spinnerType", spinnerTypeElement);
        preferencesEditor.putInt("spinnerCategory", spinnerCategoryElement);
        preferencesEditor.commit();
    }

    private void restoreDataType() {
        int spinnerTypeElementInt = preferences.getInt("spinnerType", 0);
        spinnerType.setSelection(spinnerTypeElementInt);
    }

    private void restoreDataCategory() {
        int spinnerCategoryElementInt = preferences.getInt("spinnerCategory", 0);
        spinnerCategory.setSelection(spinnerCategoryElementInt);
    }
}