package com.azmf.mrwallet;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class OpenHelper extends SQLiteOpenHelper {
    private Context context;
    OpenHelper(final Context context)
    {
        super(context, DataManager.DATABASE_NAME, null,
                DataManager.DATABASE_VERSION);  this.context = context;
    }
    @Override
    public void onOpen(final SQLiteDatabase db) {
        super.onOpen(db);
    }
    @Override
    public void onCreate(final SQLiteDatabase db) {
        MoneyTable.onCreate(db);
        CategoryTable.onCreate(db);
    }
    @Override
    public void onUpgrade(final SQLiteDatabase db,  final int oldVersion, final int newVersion) {
        MoneyTable.onUpgrade(db, oldVersion, newVersion);
        CategoryTable.onUpgrade(db, oldVersion, newVersion);
    }
}