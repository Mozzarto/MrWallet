package com.azmf.mrwallet;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.text.format.Time;
import android.widget.Toast;

public class ToastService extends Service {
    private int hour = 21;
    Handler hndlr = new Handler();

    public ToastService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        hndlr.post(runCode);
        return super.onStartCommand(intent, flags, startId);
    }

    public void pushToast() {
        Toast.makeText(getApplicationContext(), getString(R.string.toastServiceTxt), Toast.LENGTH_LONG).show();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private Runnable runCode = new Runnable() {
        @Override
        public void run() {
            Time now = new Time();
            now.setToNow();
            if(now.hour == hour && now.minute == 0)
                pushToast();
            if (now.second == 0)
                hndlr.postDelayed(runCode, 60 * 1000);
            else
                hndlr.postDelayed(runCode, (60 - now.second) * 1000);
        }
    };
}
