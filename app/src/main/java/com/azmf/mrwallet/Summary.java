package com.azmf.mrwallet;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.azmf.mrwallet.fragment.EntryFragment;
import com.azmf.mrwallet.fragment.MoneyFragment;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Summary extends AppCompatActivity {
    double yearBalance, yearIncome, yearOutgoing, monthIncome, monthOutcome, monthBalance;
    ArrayList<MoneyFragment> listIncome = new ArrayList<>();
    ArrayList<MoneyFragment> listOutcome = new ArrayList<>();
    private static DecimalFormat df2 = new DecimalFormat("0.00");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.summary);
        DataManager dataManager = DataManagerObject.dataManager;

        int year = Calendar.getInstance().get(Calendar.YEAR);
        int month = Calendar.getInstance().get(Calendar.MONTH) + 1;

        yearIncome = dataManager.getYearIncome(year);
        yearOutgoing = dataManager.getYearOutcome(year);
        yearBalance = dataManager.getYearBalance(year);
        monthIncome = dataManager.getMonthIncome(month);
        monthOutcome = dataManager.getMonthOutcome(month);
        monthBalance = dataManager.getMonthBalance(month);

        List<Money> listIncomeMoney = dataManager.getMonthIncomeList(month);
        List<Money> listOutcomeMoney = dataManager.getMonthOutcomeList(month);

        for (Money income : listIncomeMoney){
            Category category = dataManager.getCategory(income.getCategoryID());
            listIncome.add(new MoneyFragment(income.getId(), income.getAmount(), "", category.getName()));
        }

        for (Money outcome : listOutcomeMoney){
            Category category = dataManager.getCategory(outcome.getCategoryID());
            listOutcome.add(new MoneyFragment(outcome.getId(), outcome.getAmount(), "", category.getName()));
        }

        dataToView();
    }

    //Method for setting values to view
    private void dataToView() {
        TextView incomeY = findViewById(R.id.YearIncomeValue);
        incomeY.setText(doubleToString(yearIncome));

        TextView outgoingY = findViewById(R.id.YearOutcomeValue);
        outgoingY.setText(doubleToString(yearOutgoing));

        TextView balanceY = findViewById(R.id.YearBalanceValue);
        balanceY.setText(doubleToString(yearBalance));

        TextView incomeM = findViewById(R.id.MonthIncomeValue);
        incomeM.setText(doubleToString(monthIncome));

        TextView outgoingM = findViewById(R.id.MonthOutcomeValue);
        outgoingM.setText(doubleToString(monthOutcome));

        TextView balanceM = findViewById(R.id.MonthBalanceValue);
        balanceM.setText(doubleToString(monthBalance));

        EntryFragment incomeList = (EntryFragment) getSupportFragmentManager().findFragmentById(R.id.MonthIncomeList);
        if(incomeList != null)
            incomeList.passList(listIncome);

        EntryFragment outcomeList = (EntryFragment) getSupportFragmentManager().findFragmentById(R.id.MonthOutcomeList);
        if(outcomeList != null)
            outcomeList.passList(listOutcome);
    }

    private String doubleToString(double money) {
        String ret = df2.format(money);
        ret += " zł";
        return ret;
    }
}
