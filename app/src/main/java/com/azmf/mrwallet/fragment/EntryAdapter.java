package com.azmf.mrwallet.fragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.azmf.mrwallet.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EntryAdapter extends BaseAdapter implements ListAdapter {
    private ArrayList<MoneyFragment> list = new ArrayList<MoneyFragment>();
    private Context context;

    public EntryAdapter(ArrayList<MoneyFragment> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return list.get(pos).getId();
        //just return 0 if your list items do not have an Id variable.
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.entry_layout, null);
        }

        TextView moneyTV = view.findViewById(R.id.fragmentMoney);
        moneyTV.setText(doubleToString(list.get(position).getAmount()));

        String[] categoryInList = context.getResources().getStringArray(R.array.incomeCategories);
        String[] categoryOutList = context.getResources().getStringArray(R.array.outgoingCategories);

        String categoryName = list.get(position).getCategory();
        boolean hasCategory = false;

        for (String cat : categoryInList){
            if (cat.equals(categoryName)){
                hasCategory = true;
            }
        }

        for (String cat : categoryOutList){
            if (cat.equals(categoryName)){
                hasCategory = true;
            }
        }

        if (!hasCategory){
            switch (categoryName) {
                case "Other":
                    categoryName = "Inne";
                    break;
                case "Lottery":
                    categoryName = "Loteria";
                    break;
                case "Gift":
                    categoryName = "Prezent";
                    break;
                case "Sale":
                    categoryName = "Sprzedaż";
                    break;
                case "Salary":
                    categoryName = "Wypłata";
                    break;
                case "Return from a friend":
                    categoryName = "Zwrot od przyjaciela";
                    break;
                case "Tax refund":
                    categoryName = "Zwrot podatku";
                    break;
                case "Return in store":
                    categoryName = "Zwrot w sklepie";
                    break;
                case "Rent":
                    categoryName = "Czynsz";
                    break;
                case "Electronics":
                    categoryName = "Elektronika";
                    break;
                case "Food and drink":
                    categoryName = "Jedzenie i picie";
                    break;
                case "Mandate":
                    categoryName = "Mandat";
                    break;
                case "Loan installment":
                    categoryName = "Rata";
                    break;
                case "Entertainment":
                    categoryName = "Rozrywka";
                    break;
                case "Phone":
                    categoryName = "Telefon";
                    break;
                case "Health and beauty":
                    categoryName = "Zdrowie i uroda";
                    break;
                case "Clothes":
                    categoryName = "Odzież";
                    break;
                case "SMS":
                    categoryName = "SMS";
                    break;
            }
        }

        TextView categoryTV = view.findViewById(R.id.fragmentCategory);
        categoryTV.setText(categoryName);

        TextView dateTV = view.findViewById(R.id.fragmentDate);
        dateTV.setText(list.get(position).getDate());

        return view;
    }

    private String doubleToString(double money){
        int gr = (int)(money * 100);
        gr = gr % 100;
        int zl = (int)money;
        String ret;
        ret = Integer.toString(zl);
        ret += ".";
        if(gr < 10){
            ret += "0" + Integer.toString(gr);
        } else {
            ret += Integer.toString(gr);
        }
        ret += "zł";
        return ret;
    }
}
