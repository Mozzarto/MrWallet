package com.azmf.mrwallet.fragment;

import com.azmf.mrwallet.Money;

public class MoneyFragment {
    private long id;
    private double amount;
    private String date;
    private String category;

    public MoneyFragment(long id, double amount, String date, String category) {
        this.id = id;
        this.amount = amount;
        this.date = date;
        this.category = category;
    }

    public MoneyFragment(Money money, String category){
        this.id = money.getId();
        this.amount = money.getAmount();
        this.date = money.getDate();
        this.category = category;
        //TODO auto get category
    }

    public MoneyFragment() {
        this.id = id;
        this.amount = amount;
        this.date = date;
        this.category = category;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String categoryID) {
        this.category = categoryID;
    }
}
