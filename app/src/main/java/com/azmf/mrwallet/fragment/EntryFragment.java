package com.azmf.mrwallet.fragment;

import android.os.Bundle;
import android.widget.ListView;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.azmf.mrwallet.R;

import java.util.ArrayList;

public class EntryFragment extends Fragment {

    ArrayList<MoneyFragment> list;

    public EntryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        viewProccessing();
        return inflater.inflate(R.layout.entry_fragment, container, false);
    }

    private void viewProccessing(){
        if(list != null) {
            EntryAdapter adapter = new EntryAdapter(list, this.getContext());

            ListView lVIew = this.getView().findViewById(R.id.fragmentList);
            lVIew.setAdapter(adapter);
        }
    }
    public void passList(ArrayList<MoneyFragment> list){
        this.list = list;
        viewProccessing();
    }
}
