package com.azmf.mrwallet;

public class Money {
    private long id;
    private String name;
    private double amount;
    private String date;
    private long categoryID;

    public Money(double amount, String date, long categoryID) {
        this.amount = amount;
        this.date = date;
        this.categoryID = categoryID;
    }

    public Money() {
        this.amount = amount;
        this.date = date;
        this.categoryID = categoryID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(long categoryID) {
        this.categoryID = categoryID;
    }
}
